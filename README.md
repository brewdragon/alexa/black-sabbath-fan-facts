# Black Sabbath Fan Facts
Just a fun little Alexa skill to provide you with a fact about the greatest
heavy metal band of all time, Black Sabbath!

Skill link: [Black Sabbath Fan Facts](https://www.amazon.com/Kevin-Masaryk-Black-Sabbath-Facts/dp/B06XVPSMS4)

## Invocations
Example phrases you can say to Alexa to open the skill:

> *"Alexa, open Black Sabbath Fan Facts"*

> *"Alexa, give me a fact from Black Sabbath Fan Facts"*

Once activated, the skill will simply tell you a fact about Black Sabbath and
then exit. The fact text will also be printed to a "card" in the Alexa app on
your mobile device; See below for help on finding it. If you have an Alexa
device with a screen, such as an Echo Show, the text of the fact will be
printed to the screen.

## Cards
The text of each fact will printed to a "card" in the Alexa app on your mobile
device. To view the card:

1. Open the Alexa app on your mobile device.
2. Tap the "More" button at the bottom right of the screen.
3. Select "Activity History"

The card should now be visible on your screen.

## Support
For bugs, feature requests, etc. you can [open an issue](https://gitlab.com/brewdragon/alexa/black-sabbath-fan-facts/-/issues).
